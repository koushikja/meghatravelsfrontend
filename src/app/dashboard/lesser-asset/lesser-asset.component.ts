import { Component, OnInit } from '@angular/core';
import {AssetService} from "../../../services/asset.service";

@Component({
  selector: 'app-lesser-asset',
  templateUrl: './lesser-asset.component.html',
  styleUrls: ['./lesser-asset.component.scss']
})
export class LesserAssetComponent implements OnInit {
    isEdit:boolean=false;
   assetType : string = "Lesser";
   lesserAssetList:any=[];
   lesserDetails:any={}
   lesserEditDetails:any={}
  constructor(public assetService:AssetService) { }

  ngOnInit() {
     this.getAllAssets()
  }
    assetLesserAdded(){
        this.getAllAssets()
    }


    getAllAssets(){
        this.assetService.getAssetlistSpecificList( this.assetType )
            .subscribe((assetList: any) => {
                this.lesserAssetList=assetList
            })
    }

    intermerdiateDeleteTranasaction(lesserDetail){
        this.isEdit=true
       this.lesserDetails=lesserDetail
    }
    deleteLesserDetails(lesserDetail){
        this.assetService.deleteAssetDetailsByMongodbId(lesserDetail._id)
            .subscribe((assetList: any) => {
                this.deleteAssetCache(lesserDetail)
            })
    }

    deleteAssetCache(lesserDetail){
        for(let j=0;j<this.lesserAssetList.length;j++){
            if(this.lesserAssetList[j]._id===lesserDetail['_id']){
                this.lesserAssetList.splice(j,1);
            }
        }
    }
    editIntermediateLesser(lesserDetail){
       this.isEdit=true
       this.lesserEditDetails =lesserDetail
    }

    addAsset(){
       this.isEdit=false
    }
}
