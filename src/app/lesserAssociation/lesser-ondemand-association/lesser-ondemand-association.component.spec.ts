import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LesserOndemandAssociationComponent } from './lesser-ondemand-association.component';

describe('LesserOndemandAssociationComponent', () => {
  let component: LesserOndemandAssociationComponent;
  let fixture: ComponentFixture<LesserOndemandAssociationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LesserOndemandAssociationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LesserOndemandAssociationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
