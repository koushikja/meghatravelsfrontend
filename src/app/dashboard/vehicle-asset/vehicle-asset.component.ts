import { Component, OnInit } from '@angular/core';
import {AssetService} from "../../../services/asset.service";

@Component({
  selector: 'app-vehicle-asset',
  templateUrl: './vehicle-asset.component.html',
  styleUrls: ['./vehicle-asset.component.scss']
})
export class VehicleAssetComponent implements OnInit {
isEdit:boolean=false;
assetType : string = "Vehicle";
vehicleAssetList:any=[];
vehicleDetails:any={}
vehicleEditDetails:any={}
constructor(public assetService:AssetService) { }

  ngOnInit() {
     this.getAllAssets()
  }
    assetvehicleAdded(){
        this.getAllAssets()
    }


    getAllAssets(){
        this.assetService.getAssetlistSpecificList( this.assetType )
            .subscribe((assetList: any) => {
                this.vehicleAssetList=assetList
            })
    }

    intermerdiateDeleteTranasaction(vehicleDetail){
        this.isEdit=true
       this.vehicleDetails=vehicleDetail
    }
    deletevehicleDetails(vehicleDetail){
        this.assetService.deleteAssetDetailsByMongodbId(vehicleDetail._id)
            .subscribe((assetList: any) => {
                this.deleteAssetCache(vehicleDetail)
            })
    }

    deleteAssetCache(vehicleDetail){
        for(let j=0;j<this.vehicleAssetList.length;j++){
            if(this.vehicleAssetList[j]._id===vehicleDetail['_id']){
                this.vehicleAssetList.splice(j,1);
            }
        }
    }
    editIntermediatevehicle(vehicleDetail){
       this.isEdit=true
       this.vehicleEditDetails =vehicleDetail
    }

    addAsset(){
       this.isEdit=false
    }
}
