import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {ConfigService} from "./config.service";

@Injectable()
export class VechicleExpenseServiceService {

  constructor(private http: HttpClient, public configService: ConfigService){ }

    getAssetlistSpecificList(assetType) {
        return this.http.get(this.configService.appConfig.appBaseUrl + 'assetsByAssetType/' + assetType);
    }


    saveVechicleExpense(expenseData) {
        return this.http.post(this.configService.appConfig.appBaseUrl + 'saveVechicleExpense', expenseData);
    }

    getVechicleExpense(skip, limit) {
        return this.http.get(this.configService.appConfig.appBaseUrl + 'allVechicleExpense' + '/' + skip + '/' + limit);
    }

    getVechicleExpenseByMongodbId(vechicleExpenseMongoId) {
        return this.http.get(this.configService.appConfig.appBaseUrl + 'vechicleExpenseBymongoId/' + vechicleExpenseMongoId);
    }

    updateVechicleExpenseById(vechicleExpenseMongoId, expense) {
        return this.http.post(this.configService.appConfig.appBaseUrl + 'editVechicleExpense/'  + vechicleExpenseMongoId, expense);
    }



    deleteVechicleExpenseByMongodbId(vechicleExpenseMongoId) {
      console.log(vechicleExpenseMongoId)
        return this.http.delete(this.configService.appConfig.appBaseUrl + 'deleteVechicleExpense/' + vechicleExpenseMongoId);
    }

    getVechicleExpenseCount() {
        return this.http.get(this.configService.appConfig.appBaseUrl + 'allVechicleExpense/count');
    }


}
