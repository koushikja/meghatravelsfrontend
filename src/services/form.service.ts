import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Injectable()
export class FormService {
    expenseFromArr=[
        {
        "assetType": {
            "typeId": "Maintenance",
            "label": "Maintenance",
            theme: {
                color: {
                    primary: "#5C6BC0",
                    secondary: "#7986CB"
                }
            },
            order: 1
        },
        "configuration": {
            "maintenanceDesp" : {
                "type" : "text",
                "field":"maintenanceDesp",
                "description" : "Enter maintenanceDesp",
                "label":"maintenance Description",
                "required":true,
                "autoComplete":false,
                "fieldDisplayType":"textarea"
            }
            }

    },
        {
            "assetType": {
                "typeId": "FuelType",
                "label": "FuelType",
                theme: {
                    color: {
                        primary: "#5C6BC0",
                        secondary: "#7986CB"
                    }
                },
                order: 1
            },
            "configuration": {
                "numberOfLiters" : {
                    "type" : "text",
                    "field":"numberOfLiters",
                    "description" : "Enter number Of Liters",
                    "label":"Number Of Liters",
                    "required":true,
                    "autoComplete":false,
                    "fieldDisplayType":"text"
                },
                "costPerLiter" : {
                    "type" : "text",
                    "field":"costPerLiter",
                    "description" : "Enter cost Per Liter",
                    "label":"Cost Per Liter",
                    "required":true,
                    "autoComplete":false,
                    "fieldDisplayType":"text"
                },

            }
        },
        {
            "assetType": {
                "typeId": "Tyrechange",
                "label": "Tyrechange",
                theme: {
                    color: {
                        primary: "#5C6BC0",
                        secondary: "#7986CB"
                    }
                },
                order: 1
            },
            "configuration": {
                "tyreChangeDesp" : {
                    "type" : "text",
                    "field":"tyreChangeDesp",
                    "description" : "Enter tyreChangeDesp",
                    "label":"Tyre Change Description",
                    "required":true,
                    "autoComplete":false,
                    "fieldDisplayType":"textarea"
                }
            }

        },
        {
            "assetType": {
                "typeId": "Others",
                "label": "Others",
                theme: {
                    color: {
                        primary: "#5C6BC0",
                        secondary: "#7986CB"
                    }
                },
                order: 1
            },
            "configuration": {
                "OthersDesp" : {
                    "type" : "text",
                    "field":"OthersDesp",
                    "description" : "Enter OthersDesp",
                    "label":"Others Description",
                    "required":true,
                    "autoComplete":false,
                    "fieldDisplayType":"textarea"
                }
            }

        }
    ]

    constructor(private http: HttpClient) {
    }

    getExpenseTypefromConfig(){
        return this.expenseFromArr
    }

    formatAssetConfig(assetConfig) {
        let arr = []
        for (let key in assetConfig) {
            if (assetConfig.hasOwnProperty(key)) {
                arr.push(assetConfig[key])
            }
        }
        return arr;
    }

    formatEditAssetConfig(assetConfig, assetDetails) {
        let arr = []
        for (let key in assetConfig) {
            if (assetConfig.hasOwnProperty(key)) {
                assetConfig[key].fieldValue = assetDetails[key];
                arr.push(assetConfig[key])
            }
        }
        return arr;
    }


    formatEditAssetDetails(assetDetails) {
        var obj = {}
        for (let key in assetDetails) {
            if (assetDetails.hasOwnProperty(key)) {
                obj[assetDetails[key].field] = assetDetails[key].fieldValue
            }
        }
        return obj
    }

    formatAssetSaveDetails(assetDetails) {
        let assetModifiedObj = {};
        for (let key in assetDetails) {
           /* console.log("000000000if0000000000assetModifiedObj000000000000----")
            console.log(assetDetails.hasOwnProperty(key) )
            console.log( key  )
            console.log( typeof assetDetails[key]  )*/
            if (assetDetails.hasOwnProperty(key) && (typeof  assetDetails[key] === 'string'||typeof  assetDetails[key] === 'number')){
                /*console.log("000000000if0000000000assetModifiedObj000000000000----")
                console.log( assetModifiedObj[key])
                console.log( assetDetails[key])*/
                assetModifiedObj[key] = assetDetails[key]
            }
            else if (typeof  assetDetails[key] == 'object' && !assetDetails[key].hasOwnProperty("type")) {
               /* console.log("0000000000000000000000000000000----")
                console.log(assetDetails[key])*/

                const obj = assetDetails[key]
                let arr = [];
                for (let p in obj)
                    arr.push(obj[p]);
                const str = arr.join('-');
              /*  console.log("0000000000000000000")
                console.log(str)*/
                assetModifiedObj['modifieDate'] = str
                assetModifiedObj[key] = assetDetails[key]
            }
        }


        return assetModifiedObj;
    }
}
