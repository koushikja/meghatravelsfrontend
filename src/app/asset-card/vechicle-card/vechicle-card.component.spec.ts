import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {VechicleCardComponent} from './vechicle-card.component';

describe('VechicleCardComponent', () => {
    let component: VechicleCardComponent;
    let fixture: ComponentFixture<VechicleCardComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [VechicleCardComponent]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(VechicleCardComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
