import {Component, OnInit} from '@angular/core';
import * as _ from "lodash";
import {AssetService} from "../../../services/asset.service";
import {VechicleTypeServiceService} from "../../../services/vechicle-type-service.service";
import {ReportServiceService} from "../../../services/reportService/report-service.service";
import * as jsPDF from 'jspdf';
import 'jspdf-autotable';

@Component({
    selector: 'app-report',
    templateUrl: './report.component.html',
    styleUrls: ['./report.component.scss']
})
export class ReportComponent implements OnInit {
    companyAssetList:any=[]
    vehicleTypeList:any=[]
    resultDetails:any=[]
    selectedVehicleType:any=""
    selectCompany:any=""
    selectedCCType:any=""
    startDate:Date;
    endDate:Date;
    ccList:any=["OnDemand","contract type 2","Shift Type","fixedDayKmContract"]
    constructor(public assetService: AssetService, public vechicleTypeServiceService:VechicleTypeServiceService,
                public reportServiceService:ReportServiceService) {
    }


    downLoadPdf(){
        let doc = new jsPDF();
        doc.autoTable(this.columns, this.rows,  {addPageContent: function(data) {
                doc.text("", 120, 90);
            }});
        doc.save('Report.pdf')
    }

    ngOnInit() {
        this.getAllVehicleType();
        this.getAllAssets();
    }


    getAllVehicleType(){
        this.vechicleTypeServiceService.getAllVechicleTypeDropdown()
            .subscribe((vechicleTypes: any) => {
                console.log(vechicleTypes)
                this.vehicleTypeList=vechicleTypes
            })
    }

    getAllAssets(){
        this.assetService.getAssetlistSpecificList( "Company" )
            .subscribe((assetList: any) => {
                this.companyAssetList=assetList
            })
    }

    submitQuery(){

        console.log(this.selectedCCType)
        console.log(this.startDate)
        console.log(this.endDate)
        console.log(this.selectedVehicleType)
        console.log(this.selectCompany)
        this.reportServiceService.getCorporateContractReport(this.selectCompany,this.selectedCCType
        ,this.startDate,this.endDate,this.selectedVehicleType)
            .subscribe((ccListResult: any) => {
                console.log(ccListResult)

                this.resultDetails=ccListResult
                this.fromReportData()
                this.resetFrom()
            })
    }

    resetFrom(){
        this.selectedVehicleType=""
        this.selectCompany=""
        this.selectedCCType=""
    }

    columns = [];
    rows = [];
    fromReportData(){
        if(this.resultDetails.length>0){
            let objectKeys=Object.keys(this.resultDetails[0])
            for(let u=0;u<objectKeys.length;u++){
                let titleObj={}
                titleObj['title']=objectKeys[u]
                titleObj['dataKey']=objectKeys[u]
                this.columns.push(titleObj)
            }
            console.log("8888888this.columns")
            console.log(this.columns)
            for(let j=0;j< this.resultDetails.length;j++){
                console.log(this.resultDetails[j])
                this.rows.push(this.resultDetails[j])
            }
        }

    }
}



