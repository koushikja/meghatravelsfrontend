import {Injectable} from '@angular/core';
import {ConfigService} from "./config.service";
import {HttpClient} from "@angular/common/http";

@Injectable()
export class FuelServiceService {

    constructor(private http: HttpClient, public configService: ConfigService) {
    }

    saveFuelTypeDetails(fuelTypeData) {
        return this.http.post(this.configService.appConfig.appBaseUrl + 'fuelDetails', fuelTypeData);
    }

    getAllFuelType(skip, limit) {
        return this.http.get(this.configService.appConfig.appBaseUrl + 'allFuelDetails/' + skip + '/' + limit);
    }

    getFuelTypeByMongodbId(fuelTypeId) {
        return this.http.get(this.configService.appConfig.appBaseUrl + 'fuelBymongoId/' + fuelTypeId);
    }

    updateFuelTypeById(fuelTypeId, fuelTypeData) {
        console.log("inside sericev0" + fuelTypeData)
        return this.http.post(this.configService.appConfig.appBaseUrl + 'editFuelDetails/' + fuelTypeId, fuelTypeData);
    }


    deleteFuelTypeByMongodbId(fuelTypeId) {
        return this.http.delete(this.configService.appConfig.appBaseUrl + 'fuelBymongoId/' + fuelTypeId);
    }

    getFuelTypeCount() {
        return this.http.get(this.configService.appConfig.appBaseUrl + 'allFuelDetails/count');
    }


    getFuelTypeForDropDown() {
        return this.http.get(this.configService.appConfig.appBaseUrl + 'allFuelsDropDown' );
    }

}
