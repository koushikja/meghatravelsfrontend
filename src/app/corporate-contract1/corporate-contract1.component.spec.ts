import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CorporateContract1Component } from './corporate-contract1.component';

describe('CorporateContract1Component', () => {
  let component: CorporateContract1Component;
  let fixture: ComponentFixture<CorporateContract1Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CorporateContract1Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CorporateContract1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
