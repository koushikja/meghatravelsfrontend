import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VehicleCorporteAssociation1Component } from './vehicle-corporte-association1.component';

describe('VehicleCorporteAssociation1Component', () => {
  let component: VehicleCorporteAssociation1Component;
  let fixture: ComponentFixture<VehicleCorporteAssociation1Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VehicleCorporteAssociation1Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VehicleCorporteAssociation1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
