import { TestBed, inject } from '@angular/core/testing';

import { VechicleHistoryService } from '../services/vechicle-history.service';

describe('VechicleHistoryService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [VechicleHistoryService]
    });
  });

  it('should be created', inject([VechicleHistoryService], (service: VechicleHistoryService) => {
    expect(service).toBeTruthy();
  }));
});
