import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ShiftTypeCostComponent} from './shift-type-cost.component';

describe('ShiftTypeCostComponent', () => {
    let component: ShiftTypeCostComponent;
    let fixture: ComponentFixture<ShiftTypeCostComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [ShiftTypeCostComponent]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(ShiftTypeCostComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
