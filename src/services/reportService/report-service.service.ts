import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {ConfigService} from "../config.service";

@Injectable()
export class ReportServiceService {

    constructor(private http: HttpClient, public configService: ConfigService) {
    }
    getCorporateContractReport(companyName,cctype,startDate,endDate,vehicleType) {
        if(companyName!==""){
            return this.http.get(this.configService.appConfig.appBaseUrl + 'transactionReport/?companyName='+companyName);
        }
        if(cctype!=="") {
            return this.http.get(this.configService.appConfig.appBaseUrl + 'transactionReport/?cctype='+cctype);
        }
        if(cctype!=="" && companyName!=="" ) {
            return this.http.get(this.configService.appConfig.appBaseUrl + 'transactionReport/?cctype='+cctype+'&companyName='+companyName);
        }



    }
}
/*
+'&companyName='+companyName+'&startDate='+startDate+'&startDate='+endDate+'&vehicleType='+vehicleType*/
