import { Component, OnInit } from '@angular/core';
import {ShiftTypeServiceService} from "../../services/shift-type-service.service";
import {VechicleExpenseServiceService} from "../../services/vechicle-expense-service.service";
import {FormService} from "../../services/form.service";
import {AssetService} from "../../services/asset.service";

@Component({
  selector: 'app-vehicle-expense',
  templateUrl: './vehicle-expense.component.html',
  styleUrls: ['./vehicle-expense.component.scss']
})
export class VehicleExpenseComponent implements OnInit {
    allAssets:any
    expenseEditDetails:any
    vechicleExpenseObj:any={


    }
    public pagination: any;
    vehicleNo:any;
    expenseFromField:any;
    expenseDescription:any;
    expenseType:any;
    expenseDate:any;
    showEditFrom:boolean=false;


  constructor(public vechicleExpenseServiceService: VechicleExpenseServiceService,
              public formService:FormService ,public assetService:AssetService ){

      this.pagination = {
          'currentPage': 1,
          'totalPageCount': 0,
          'recordsPerPage': 12
      };
  }

    getVechicleList(){
        this.assetService.getAssetlistSpecificList("Vehicle")
            .subscribe((vechicleList: any) => {
                console.log("******vechicleList*************");
                console.log(vechicleList)
                this.allAssets =vechicleList
            })
    }

 /*  getVechicleList(){
       this.vechicleExpenseServiceService.getAssetlistSpecificList("Vehicle")
           .subscribe(allAssets => {
               this.allAssets = allAssets;

           });

    }*/
    submitExpenseDetails(){
        var obj={}
        obj["vehicleNo"]=this.vehicleNo
        obj["expenseType"]=this.expenseType
        obj["expenseDescription"]=this.expenseDescription
        obj["expenseDate"]=this.expenseDate
        obj["vehicleExpenseDetails"]  =this.formService.formatAssetSaveDetails(this.expenseFromField)

        this.vechicleExpenseServiceService.saveVechicleExpense(obj)
            .subscribe((data: any) => {
                this.getAssetTotalCountForPagination()
                this.getExpenseBasedOnrange(0, 10)
            });
    }

  ngOnInit() {
      this.getAssetTotalCountForPagination()
      this.getExpenseBasedOnrange(0, 10)
  }

    getAssetTotalCountForPagination() {
        this.vechicleExpenseServiceService.getVechicleExpenseCount()
            .subscribe((userCount: any) => {
                this.pagination.totalPageCount = userCount.count;
            });
    }

    getExpenseBasedOnrange(skip, limit) {
        this.vechicleExpenseServiceService.getVechicleExpense(skip, limit)
            .subscribe(allAssets => {
                console.log("allAssets1111111111111111111")
                console.log(allAssets)
                this.allAssets =   this.restructureResponse(allAssets);

            });
    }

    restructureResponse(allAssets){
      for(var j=0;j<allAssets.length;j++){
          console.log("allAssets1111111111111111111")
          console.log(allAssets[j].expenseDate)
          const obj = allAssets[j].expenseDate
          let arr = [];
          for (let p in obj)
              arr.push(obj[p]);
          const str = arr.join('-');
          console.log("0000000000000000000")
          console.log(str)
          allAssets[j]['modifieDate'] = str
      }
      return allAssets;

    }

    getEditConfiguration(expenseDetails){

        this.vechicleExpenseServiceService.getVechicleExpenseByMongodbId(expenseDetails._id)
            .subscribe(expenseData => {
                this.showEditFrom=true
                console.log("allAssets1111111111111111111")
                console.log(expenseData)
                this.expenseEditDetails=expenseData
            });
    }


    updateExpenseDetails(expenseDetails){
        this.vechicleExpenseServiceService.updateVechicleExpenseById(expenseDetails._id,expenseDetails)
            .subscribe(expenseData => {
                this.showEditFrom=true
                console.log("allAssets1111111111111111111")
                console.log(expenseData)
                this.expenseEditDetails=expenseData
                this.getAssetTotalCountForPagination()
                this.getExpenseBasedOnrange(0, 10)
            });
    }

    deleteExpenseDetails(expenseDetails){
        this.vechicleExpenseServiceService.deleteVechicleExpenseByMongodbId(expenseDetails['_id'])
            .subscribe(expenseData => {
                console.log("allAssets1111111111111111111")
                console.log(expenseData)
                this.getAssetTotalCountForPagination()
                this.getExpenseBasedOnrange(0, 10)
            });

    }
    deleteTransactionData:any
    intermerdiateDeleteTranasaction(transactionData){
        this.deleteTransactionData=transactionData
    }


    getFromFields(){
        console.log("allAssets1111111111111111111")
        console.log(this.expenseType)
        this.expenseType = this.expenseType.replace(/ +/g, "");
     let expenseFrom=this.formService.getExpenseTypefromConfig();
        console.log(expenseFrom)
        for(let h=0;h<expenseFrom.length;h++){
            if(expenseFrom[h].assetType.typeId===this.expenseType){
                let configObj=  expenseFrom[h].configuration
                let responseKeys = Object.keys(configObj);
                let formData = [];
                for ( var prop of responseKeys) {
                    formData.push( configObj[prop]);
                }
                console.log("allAssets1111111111111111111")
                console.log(formData)
                this.expenseFromField=  formData
            }
        }

    }


}
