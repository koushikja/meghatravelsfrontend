import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VechicleTypeComponent } from './vechicle-type.component';

describe('VechicleTypeComponent', () => {
  let component: VechicleTypeComponent;
  let fixture: ComponentFixture<VechicleTypeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VechicleTypeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VechicleTypeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
