import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DriverAssetFromComponent } from './driver-asset-from.component';

describe('DriverAssetFromComponent', () => {
  let component: DriverAssetFromComponent;
  let fixture: ComponentFixture<DriverAssetFromComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DriverAssetFromComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DriverAssetFromComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
