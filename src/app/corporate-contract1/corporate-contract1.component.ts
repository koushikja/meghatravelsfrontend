import { Component, OnInit } from '@angular/core';
import {VechicleTransactionService} from "../../services/vechicle-transaction.service";
import {FormService} from "../../services/form.service";
import {ApplicationParameteConfigService} from "../../services/application-paramete-config.service";
import {ShiftTypeServiceService} from "../../services/shift-type-service.service";
import {VechicleTypeServiceService} from "../../services/vechicle-type-service.service";
import {AssetService} from "../../services/asset.service";
import {VehicleCorporateContractService} from "../../services/vehicle-corporate-contract.service";
import {CorporateContractFieldsService} from "../../services/corporate-contract-fields.service";
import {CctransactionService} from "../../services/cctransaction.service";
import {Subscription} from "rxjs";
import {ComponetCommunicationService} from "../../services/componet-communication.service";
import * as _ from 'lodash'
import {ExcelService} from "../../services/excel.service";
import * as XLSX from "xlsx";

@Component({
  selector: 'app-corporate-contract1',
  templateUrl: './corporate-contract1.component.html',
  styleUrls: ['./corporate-contract1.component.scss']
})
export class CorporateContract1Component implements OnInit {
    monthList=["January","February","March","April","May","June","July","August"
        ,"September","October","November","December"]
    allVechicleTypeDropDown:any;
    selectedTransactionMonth:any;
    companyAssetDropDown:any;
    vehicleTypeSelected:any;
    selectedVehicleItems:any;
    companySelected:any;
    corporateContractFields:any;
    formConfigData:any={};
    bulkUpload:any={}
    public vechicleDropdownList = [];
    public CCList:any = [];
    public vechicleSelectedDetails = {};
    public vechicleSelectedVehicleType = "";
    public vechicleTransactionSelected = "";
    public companyTransactionSelected= "";
    corporateContractType:any= "contract type 1"
    companyDropdownSettings = {
        singleSelection:false,
        idField: '_id',
        textField: 'companySelectedForDropDown',
        selectAllText: 'Select All',
        unSelectAllText: 'UnSelect All',
        allowSearchFilter: true,
        closeDropDownOnSelection:true
    }
    public vechicleContract1dropdownSettings = {
        singleSelection:false,
        idField: '_id',
        textField: 'vehicleNumber',
        allowSearchFilter: true,
        closeDropDownOnSelection:true,

    };

  /*  selectMonth(month){
        console.log("*********************"+month)
        this.selectedTransactionMonth=month[0]

    }*/

    subscription: Subscription;
    constructor( public assetService:AssetService,
                 public vehicleCorporateContractService:VehicleCorporateContractService,
                 public corporateContractFieldsService:CorporateContractFieldsService,
                 public cctransactionService:CctransactionService,
                 public componetCommunicationService: ComponetCommunicationService,
                 public vechicleTypeServiceService:VechicleTypeServiceService,
                 public excelService:ExcelService){
            this.subscription = this.componetCommunicationService.getCCSelected().subscribe(ccType => {
            this.corporateContractType=ccType


        });
    }

    getIncialConfig(){
        this.getAllCompanyOfTypeCC1();
        this.getAllVechicleAsset();
        this.getCCIncialConfigData();
    }


    getAllCompanyOfTypeCC1(){
        this.vehicleCorporateContractService.getCompanyByCCType(this.corporateContractType)
            .subscribe((companyList: any) => {
                this.companyAssetDropDown =_.uniqBy(companyList,function(companyList){ return companyList['companySelectedForDropDown']});
            })
    }
    getAllVechicleAsset(){
        this.assetService.getAssetlistSpecificList("Vehicle")
            .subscribe((vechicleList: any) => {
                this.vechicleDropdownList =vechicleList
            })
    }
    getCorporateContractBasedOnrange(skip, limit) {
        this.vehicleCorporateContractService.getAllVehicleCorporateContract(skip, limit)
            .subscribe(corporateContractDetails => {
                this.companyAssetDropDown =corporateContractDetails
            });
    }

    onDeSelectVehicle(){
        this.vechicleTransactionSelected=""
        this.vechicleSelectedDetails=""
        this.vechicleSelectedVehicleType=""
    }
    onvehicleSelect(vehicleDetails){
        this.vechicleTransactionSelected=vehicleDetails
        this.assetService.getAssetDetailsByMongodbId(vehicleDetails._id)
            .subscribe((vehicleDetails: any) => {

                console.log(vehicleDetails)
                this.vechicleSelectedDetails=vehicleDetails
                this.vechicleSelectedVehicleType=vehicleDetails.vehicleType

            })
    }
    onCompanyDeSelect(companyDetails){
    this.getCCIncialConfigData();
    }

    onVehicleDeSelect(companyDetails){
        this.getCCIncialConfigData();
    }


    onCompanySelect(companyDetails){
        this.companyTransactionSelected=companyDetails
        this.vehicleCorporateContractService.getVehicleCorporateContractByMongodbId(companyDetails['_id'])
            .subscribe((companyCC: any) => {
                console.log("this.onCompanySelect-=--------this.vechicleSelectedVehicleType----onCompanySelect--------")
                console.log(this.vechicleSelectedDetails)
                console.log(this.vechicleSelectedDetails['vehicleType'])
                console.log(companyCC[0].companySelected._id)
                this.vehicleCorporateContractService.getcompanyCCByMongodbIdAndCCTypeAndvehicleType( companyCC[0].companySelected._id ,this.corporateContractType
                    ,this.vechicleSelectedVehicleType)
                    .subscribe((companyCCDetails: any) => {
                        if(companyCCDetails && companyCCDetails[0]){
                            companyCCDetails=companyCCDetails[0];
                            for(var g=0;g<this.corporateContractFieldsService.corporateContractTypes.length;g++){
                                if(this.corporateContractFieldsService.corporateContractTypes[g]['assetType']['typeId']==
                                    companyCCDetails.corporateContract){
                                    let CCFieldsConfiguration = Object.keys(this.corporateContractFieldsService.corporateContractTypes[g].configuration);
                                    let companyCCKeys = Object.keys(companyCCDetails);
                                    let VehicleCCKeys = Object.keys(this.vechicleSelectedDetails);
                                    if(companyCCKeys){
                                        for(var k=0;k<companyCCKeys.length;k++){
                                            for(let p=0;p<CCFieldsConfiguration.length;p++){
                                                if(companyCCKeys[k]== CCFieldsConfiguration[p]){
                                                    this.formConfigData
                                                        [this.corporateContractFieldsService.corporateContractTypes[g].configuration[CCFieldsConfiguration[p]]['field']] =
                                                        companyCCDetails[companyCCKeys[k]]
                                                }
                                            }
                                        }
                                        this.formConfigData['vehicleType']=this.vechicleSelectedVehicleType;
                                    }
                                    console.log(this.corporateContractFieldsService.corporateContractTypes[g])
                                    this.fromConfigObjectForFrom(this.corporateContractFieldsService.corporateContractTypes[g],companyCCDetails)
                                }

                            }
                        }
                        else {
                            alert("selected vehicle type   is not configured")
                        }
                    });
            })


    }



    submitTransaction(){
        console.log("************this.selectedTransa3333333333333333333ctionMonth")
        console.log(this.companyTransactionSelected)
        let obj={}

        obj['RatePerTrip']=this.formConfigData['RatePerTrip']
        obj['TDS']=this.formConfigData['TDS']
        obj['gps']=this.formConfigData['gps']
        obj['numberOfTrips']=this.formConfigData['numberOfTrips']
        obj['corporateContract']=this.corporateContractType
        obj['transactionStartDate']=this.bulkUpload.transactionStartDate
        obj['transactionEndDate']=this.bulkUpload.transactionEndDate
        obj['vehicleNumber']=this.vechicleTransactionSelected['vehicleNumber']
        obj['vehicleType']=this.formConfigData['vehicleType']
        obj['companyTransactionSelected']=this.companyTransactionSelected['companySelectedForDropDown']
        obj['totalAmt']=((this.formConfigData['numberOfTrips']*this.formConfigData['RatePerTrip']))
        obj['payable']=((this.formConfigData['numberOfTrips']*this.formConfigData['RatePerTrip'])+
            (this.formConfigData['ExtradutyRate'])-this.formConfigData['TDS']-this.formConfigData['gps']);
        console.log("88888888888888888888888888------finalll-------");
        console.log(obj);
        this.cctransactionService.saveCCTransaction(obj)
            .subscribe((data: any) => {
                this.corporateContractFields=[];
                this.formConfigData={};
                console.log("88888888888888888888888888------finalll-------");
                console.log(data);
                this.getCCTransaction()
            });

    }


    getCCIncialConfigData(){
        this.selectedVehicleItems=""
        this.companySelected=""
        this.corporateContractFields=[];
        this.companyTransactionSelected=""
        this.vechicleTransactionSelected=""
        this.vechicleSelectedDetails=""
        this.vechicleSelectedVehicleType=""
    }



    getAllVechicleType(){
        this.vechicleTypeServiceService.getAllVechicleTypeDropdown()
            .subscribe((vechicleTypes: any) => {
                console.log(vechicleTypes)
                this.allVechicleTypeDropDown=vechicleTypes
            })
    }
    fromConfigObjectForFrom(configDetailsObj,companyCCDetails){
        let configObj=  configDetailsObj.configuration
        let responseKeys = Object.keys(configObj);
        let formData = [];
        for ( var prop of responseKeys) {
            formData.push(configObj[prop]);
        }
        if(formData){
            console.log("********************this.corporateContractFields")
            console.log(this.corporateContractFields)
            this.corporateContractFields=formData;
        }


    }

    getCCTransaction() {
        this.cctransactionService.getCCTransactionByType(this.corporateContractType)
            .subscribe(CCListDetails => {
                this.CCList =CCListDetails
            });
    }


    deleteCC1Trasnaction(ccDetails){
        this.cctransactionService.deleteCCTransactionByMongodbId(ccDetails._id)
            .subscribe((data: any) => {
                this.getCCTransaction()
            })
    }
    deleteTransactionData:any
    intermerdiateDeleteTranasaction(transactionData){
    this.deleteTransactionData=transactionData
    }

    data: any = [{
        SlNo: 0,
        date: "",
        ratePerKm: 0,
        vehicleNumber: "",
        tsno:"",
        mode:0,
        company:"",
        totalKms:0,
        totalDaysPerHour:0
    }];

    exportAsXLSX():void {
        this.excelService.exportAsExcelFile(this.data, 'cc1SampleFile');
    }


    listData:any
    listIntermediateData:any
    tableDataObj:any=[];
    cc1ExcelDetails:any;
    uploadCc1ExcelDetails(event){
        let that=this
        let tableObjs;
        let input = event   .target;
        let reader = new FileReader();
        reader.onload = function(){
            let fileData = reader.result;
            let wb = XLSX.read(fileData, {type : 'binary'});
            wb.SheetNames.forEach(function(sheetName){
                let rowObj =XLSX.utils.sheet_to_json(wb.Sheets[sheetName]);
                let tableObjs=JSON.stringify(rowObj);

                if (tableObjs == undefined || tableObjs== null){
                    return;
                }
                else{
                    console.log(JSON.stringify(rowObj))
                    that.listData=JSON.stringify(rowObj)
                    that.saveIntermediate(that.listData)
                }
            })
        };
        reader.readAsBinaryString(input.files[0])


    }


    saveIntermediate(listData){
        console.log("intermediate")
        this.listIntermediateData=listData
    }

    saveCC1DetaisToDB(){
        let listData=this.listIntermediateData

        console.log("saveCC1DetaisToDB-----------listData-------")

        var arrayListData = JSON.parse("[" + listData + "]");
        for(let p=0;p<arrayListData[0].length;p++){
            console.log("saveCC1DetaisToDB------------------")
            console.log(arrayListData[0])
            this.vehicleCorporateContractService.getcompanyCCByCompanyAndCCTypeAndVehicleType(
                arrayListData[0][p]['company'],this.corporateContractType
                ,arrayListData[0][p]['mode'])
                .subscribe((companyCCDetails: any) =>{
                    console.log("companyCCDetails------------------")
                    console.log(companyCCDetails[0])
                    if(companyCCDetails[0]){
                        let obj={}
                      /*  obj['ExtradutyRate']=this.formConfigData['ExtradutyRate']*/
                        obj['RatePerTrip']=companyCCDetails[0]['RatePerTrip']
                        obj['TDS']=companyCCDetails[0]['TDS']
                        obj['gps']=arrayListData[0][p]['gps']
                        obj['numberOfTrips']=arrayListData[0][p]['tripsPerDay']
                        obj['corporateContract']=this.corporateContractType
                        obj['vehicleNumber']=arrayListData[0][p]['vehicleNumber']
                        obj['vehicleType']=arrayListData[0][p]['mode']
                        obj['transactionStartDate']=this.bulkUpload.transactionStartDate
                        obj['transactionEndDate']=this.bulkUpload.transactionEndDate
                        obj['companyTransactionSelected']=arrayListData[0][p]['company']
                        obj['totalAmt']=((arrayListData[0][p]['tripsPerDay']*companyCCDetails[0]['RatePerTrip']))
                        obj['payable']=((arrayListData[0][p]['tripsPerDay']*companyCCDetails[0]['RatePerTrip'])
                            -companyCCDetails[0]['TDS']-arrayListData[0][p]['gps']);
                        console.log("88888888888888888888888888------finalll-------");
                        console.log(obj);
                        this.cctransactionService.saveCCTransaction(obj)
                            .subscribe((data: any) => {
                                this.corporateContractFields=[];
                                this.formConfigData={};
                                console.log("88888888888888888888888888------finalll-------");
                                console.log(data);
                                this.getCCTransaction()
                            });
                    }
                    else{
                        alert(arrayListData[0][p]['company']+"is not configured of type"+arrayListData[0][p]['mode']);
                    }
                })
        }
    }



    ngOnInit() {
        this.getAllVechicleType();
        this.getCCTransaction();

    }


}
