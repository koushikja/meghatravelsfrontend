import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";

@Component({
  selector: 'app-asset-button',
  templateUrl: './asset-button.component.html',
  styleUrls: ['./asset-button.component.scss']
})
export class AssetButtonComponent implements OnInit {

  constructor(public router:Router) { }

  ngOnInit() {
  }

    navigateVehicleAsset() {
        this.router.navigate(['vehicleAsset']);
    }
    navigateDriverAsset() {
        this.router.navigate(['DriverAsset']);
    }
    navigateCompanyAsset() {
        this.router.navigate(['companyAsset']);
    }
    navigateLesserAsset() {
        this.router.navigate(['lesserAsset']);
    }

}
