import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {ConfigService} from "./config.service";

@Injectable()
export class ExpenseTypeServiceService {

    constructor(private http: HttpClient, public configService: ConfigService) {
    }

    saveExpenseTypeDetails(expenseTypeData) {
        return this.http.post(this.configService.appConfig.appBaseUrl + 'expenseDetails', expenseTypeData);
    }

    getAllExpenseType(skip, limit) {
        return this.http.get(this.configService.appConfig.appBaseUrl + 'allExpenseDetails' + '/' + skip + '/' + limit);
    }

    getExpenseTypeByMongodbId(expenseMongoId) {
        return this.http.get(this.configService.appConfig.appBaseUrl + 'expenseBymongoId/' + expenseMongoId);
    }

    updateExpenseTypeById(expenseTypeId, expenseTypeData) {
        return this.http.post(this.configService.appConfig.appBaseUrl + 'editExpenseBymongoId/' + expenseTypeId, expenseTypeData);
    }


    deleteExpenseTypeByMongodbId(expenseMongoId) {
        return this.http.delete(this.configService.appConfig.appBaseUrl + 'expenseBymongoId/' + expenseMongoId);
    }

    getExpenseTypeCostCount() {
        return this.http.get(this.configService.appConfig.appBaseUrl + 'allExpenseDetails/count');
    }

}
