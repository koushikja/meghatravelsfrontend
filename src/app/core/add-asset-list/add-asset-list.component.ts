import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
    selector: 'app-add-asset-list',
    templateUrl: './add-asset-list.component.html',
    styleUrls: ['./add-asset-list.component.scss']
})
export class AddAssetListComponent implements OnInit {
    @Input() assetTypesWithLabelsData: any;
    @Output() assetToAdd: EventEmitter<any> = new EventEmitter();
    public showSlider: boolean = false;
    public assetSelectedFromList : any

    constructor() {
    }

    ngOnInit() {
        console.log("assetTypesWithLabels", this.assetTypesWithLabelsData);
    }




    assetSelected(assetType) {
        console.log("00000000000000000assetType---------------------")
        console.log(assetType)
        this.assetSelectedFromList=assetType
        /*this.assetToAdd.emit(assetType);*/
    }

    assetTypeToAdd(){
        console.log("888888888888savedCompanyAssetCard8888888888")
    }

    getIcon(type) {
        switch (type) {

            case 'Vehicle':
                return '../../assets/asset-icons/car.png';
            case 'Lesser':
                return '../../assets/asset-icons/conference-background-selected.png';
            case 'Company':
                return '../../assets/asset-icons/organization.png';
            case 'Driver':
                return '../../assets/asset-icons/driver.png';
        }
    }

    getBackgroundColorLeftCard(type) {
        switch (type) {
            case 'Vehicle':
                return '#7986CB';
            case 'Lesser':
                return '#4DB6AC';
            case 'Company':
                return '#81C784';
            case 'Driver':
                return '#d27f72c2';


        }
    }

}
