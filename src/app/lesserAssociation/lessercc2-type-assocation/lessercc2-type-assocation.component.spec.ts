import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Lessercc2TypeAssocationComponent } from './lessercc2-type-assocation.component';

describe('Lessercc2TypeAssocationComponent', () => {
  let component: Lessercc2TypeAssocationComponent;
  let fixture: ComponentFixture<Lessercc2TypeAssocationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Lessercc2TypeAssocationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Lessercc2TypeAssocationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
