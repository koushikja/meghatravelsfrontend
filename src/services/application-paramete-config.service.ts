import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {ConfigService} from "./config.service";

@Injectable()
export class ApplicationParameteConfigService {

    constructor(private http: HttpClient, public configService: ConfigService) {
    }

    getApplicationParameterConfig(parameterName) {
        return this.http.get(this.configService.appConfig.appBaseUrl + 'applicationParameterConfig' + '?parameterType=' + parameterName);
    }

}
