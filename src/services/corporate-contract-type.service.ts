import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {ConfigService} from "./config.service";

@Injectable()
export class CorporateContractTypeService {

    cc1ContractDetails:any=[]

    constructor(private http: HttpClient, public configService: ConfigService) {
    }

    saveCorporateContractTypeDetails(CorporateContractTypeData) {
        return this.http.post(this.configService.appConfig.appBaseUrl + 'corporateContractType', CorporateContractTypeData);
    }

    getAllCorporateContractType(skip, limit) {
        return this.http.get(this.configService.appConfig.appBaseUrl + 'corporateContractType/' + skip + '/' + limit);
    }

    getCorporateContractTypeByMongodbId(CorporateContractTypeDataId) {
        return this.http.get(this.configService.appConfig.appBaseUrl + 'corporateContractModelById/' + CorporateContractTypeDataId);
    }

    updateCorporateContractTypeById(CorporateContractTypeDataIdId, CorporateContractTypeDataIdIdData) {
        return this.http.post(this.configService.appConfig.appBaseUrl + 'editcorporateContractModel/' + CorporateContractTypeDataIdId, CorporateContractTypeDataIdIdData);
    }


    deleteCorporateContractTypeByMongodbId(CorporateContractTypeDataIdIdDataId) {
        return this.http.delete(this.configService.appConfig.appBaseUrl + 'corporateContractModelByiD/' + CorporateContractTypeDataIdIdDataId);
    }

    getCorporateContractTypeCount() {
        return this.http.get(this.configService.appConfig.appBaseUrl + 'corporateContractType/count');
    }

    setCC1ContractValues(cc1Details){
        this.cc1ContractDetails=cc1Details

    }
    getCC1ContractValues(){
        return this.cc1ContractDetails;
    }

}
