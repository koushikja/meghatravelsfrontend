import { Component, OnInit } from '@angular/core';
import {AssetService} from "../../../services/asset.service";

@Component({
  selector: 'app-company-asset',
  templateUrl: './company-asset.component.html',
  styleUrls: ['./company-asset.component.scss']
})
export class CompanyAssetComponent implements OnInit {
isEdit:boolean=false;
assetType : string = "Company";
companyAssetList:any=[];
companyDetails:any={}
companyEditDetails:any={}

  constructor(public assetService:AssetService) { }

  ngOnInit() {
     this.getAllAssets()
  }
    assetcompanyAdded(){
        this.getAllAssets()
    }


    getAllAssets(){
        this.assetService.getAssetlistSpecificList( this.assetType )
            .subscribe((assetList: any) => {
                this.companyAssetList=assetList
            })
    }

    intermerdiateDeleteTranasaction(companyDetail){
        this.isEdit=true
       this.companyDetails=companyDetail
    }
    deletecompanyDetails(companyDetail){
        this.assetService.deleteAssetDetailsByMongodbId(companyDetail._id)
            .subscribe((assetList: any) => {
                console.log(assetList)
                this.deleteAssetCache(companyDetail)
            })
    }

    deleteAssetCache(companyDetail){
        for(let j=0;j<this.companyAssetList.length;j++){
            console.log("7897897897")

            if(this.companyAssetList[j]._id===companyDetail['_id']){
                console.log(this.companyAssetList[j]._id)
                console.log(companyDetail['_id'])
                this.companyAssetList.splice(j,1);
            }
        }
    }
    editIntermediatecompany(companyDetail){
       this.isEdit=true
       this.companyEditDetails =companyDetail
    }

    addAsset(){
       this.isEdit=false
    }
}