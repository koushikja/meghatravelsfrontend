import { Component, OnInit } from '@angular/core';
import {CctransactionService} from "../../../services/cctransaction.service";
import {AssetService} from "../../../services/asset.service";
import {VechicleTypeServiceService} from "../../../services/vechicle-type-service.service";
import {VehicleCorporateContractService} from "../../../services/vehicle-corporate-contract.service";
import {CorporateContractFieldsService} from "../../../services/corporate-contract-fields.service";
import * as _ from "lodash";
import * as FileSaver from 'file-saver';
import * as XLSX from 'xlsx';
import {CorporateContractTypeService} from "../../../services/corporate-contract-type.service";
import {ExcelService} from "../../../services/excel.service";


@Component({
  selector: 'app-on-demand-corporate-contract',
  templateUrl: './on-demand-corporate-contract.component.html',
  styleUrls: ['./on-demand-corporate-contract.component.scss']
})
export class OnDemandCorporateContractComponent implements OnInit {
    corporateContractType:any= "OnDemand"
    deleteTransactionData:any
    companyAssetDropDown:any;
    bulkUpload:any={}
    public CC3List:any=[];
    companyTransactionSelected:any={}
    public vechicleSelectedDetails = {};
    vechicleDropdownList:any=[];
    public vechicleTransactionSelected = "";
    public selectedVehicleItems = "";
    public companySelected = "";
    formConfigData:any={}
    transactionDate:any;
    corporateContractFields:any;
    public vechicleSelectedVehicleType = "";
    companyDropdownSettings = {
        singleSelection: false,
        idField: '_id',
        textField: 'companySelectedForDropDown',
        selectAllText: 'Select All',
        unSelectAllText: 'UnSelect All',
        itemsShowLimit: 1,
        allowSearchFilter: true,
        closeDropDownOnSelection:true
    }
    vechicledropdownSettings = {
        singleSelection: false,
        idField: '_id',
        textField: 'vehicleNumber',
        selectAllText: 'Select All',
        unSelectAllText: 'UnSelect All',
        itemsShowLimit: 1,
        allowSearchFilter: true
    };
    getIncialConfig(){
        this.bulkUpload.transactionStartDate=new Date().toJSON().slice(0,10);
        this.bulkUpload.transactionEndDate=new Date().toJSON().slice(0,10);
        this.getAllCompanyOfTypeCC3();
        this.getAllVechicleAsset();
        this.getCCIncialConfigData();
    }


    listData:any
    listIntermediateData:any
    tableDataObj:any=[];
    cc1ExcelDetails:any;
    uploadCc1ExcelDetails(event){
        let that=this
        let tableObjs;
        let input = event   .target;
        let reader = new FileReader();
        reader.onload = function(){
            let fileData = reader.result;
            let wb = XLSX.read(fileData, {type : 'binary'});
            wb.SheetNames.forEach(function(sheetName){
                let rowObj =XLSX.utils.sheet_to_json(wb.Sheets[sheetName]);
                let tableObjs=JSON.stringify(rowObj);

                if (tableObjs == undefined || tableObjs== null){
                    return;
                }
                else{
                    console.log(JSON.stringify(rowObj))
                    that.listData=JSON.stringify(rowObj)
                    that.saveIntermediate(that.listData)
                }
            })
        };
        reader.readAsBinaryString(input.files[0])


    }
    saveIntermediate(listData){
        console.log("intermediate")
        this.listIntermediateData=listData
    }

    saveCC1DetaisToDB(){
        var arrayListData = JSON.parse("[" + this.listIntermediateData + "]");
        for(let p=0;p<arrayListData[0].length;p++){
            this.vehicleCorporateContractService.getcompanyCCByCompanyAndCCTypeAndVehicleType(
                arrayListData[0][p]['company'],this.corporateContractType
                ,arrayListData[0][p]['mode'])
                .subscribe((companyCCDetails: any) =>{
                    console.log("companyCCDetails------------------")
                    console.log(companyCCDetails[0])
                    if(companyCCDetails[0]){
                        let obj={}
                        obj['date']=arrayListData[0][p]['date']
                        obj['ratePerHr']=companyCCDetails[0]['ratePerHr']
                        obj['ratePerKm']=companyCCDetails[0]['ratePerKm']
                        obj['totalDaysPerHour']=arrayListData[0][p]['totalDaysPerHour']
                        obj['vehicleType']=arrayListData[0][p]['mode']
                        obj['bookedBy']=arrayListData[0][p]['bookedBy']
                        obj['totalKms']=arrayListData[0][p]['totalKms']
                        obj['vehicleNumber']=arrayListData[0][p]['vehicleNumber']
                        obj['tsno']=arrayListData[0][p]['tsno']
                      /*  obj['transactionStartDate']=this.bulkUpload.transactionStartDate
                        obj['transactionEndDate']=this.bulkUpload.transactionEndDate*/
                        obj['corporateContract']=this.corporateContractType
                        obj['companyTransactionSelected']=arrayListData[0][p]['company']

                        if(arrayListData[0][p]['totalDaysPerHour']>=1 && arrayListData[0][p]['totalDaysPerHour']<=6 && arrayListData[0][p]['totalKms']>40){
                            obj['extraKms']=arrayListData[0][p]['totalKms']-40
                        }

                       else if(arrayListData[0][p]['totalDaysPerHour']>6&& arrayListData[0][p]['totalKms']>80){
                            obj['extraKms']=arrayListData[0][p]['totalKms']-80
                        }
                        else{
                            obj['extraHrs']=0
                        }



                         if(arrayListData[0][p]['totalDaysPerHour']>=1 && arrayListData[0][p]['totalDaysPerHour']<=6){
                            obj['slabRate']=companyCCDetails[0]['slabRate4hrs40kmts']
                        }
                        else if(arrayListData[0][p]['totalDaysPerHour']>=6){
                            obj['slabRate']=companyCCDetails[0]['slabRate8hrs80kmts']
                        }



                        if(arrayListData[0][p]['totalDaysPerHour']>=1 && arrayListData[0][p]['totalDaysPerHour']<=6  ){
                            obj['extraHrs']=arrayListData[0][p]['totalDaysPerHour']-4
                        }


                       else if(arrayListData[0][p]['totalDaysPerHour']>6){
                            obj['extraHrs']=Math.abs(arrayListData[0][p]['totalDaysPerHour']-8)
                        }
                        else{
                            obj['extraHrs']=0
                        }
                        obj['extraKmAmt']=companyCCDetails[0]['ratePerKm']*obj['extraKms']
                        obj['extraHrAmt']=companyCCDetails[0]['ratePerHr']*obj['extraHrs']
                        obj['tripAmt']=obj['extraKmAmt']+ obj['extraHrAmt']+ obj['slabRate'];
                        this.cctransactionService.saveCCTransaction(obj)
                            .subscribe((data: any) => {
                                console.log("-0-09-9-09909==========After savee======obj=====")
                                console.log(data)
                                this.getCCTransaction();
                            });
                    }

                    else{
                        alert(arrayListData[0][p]['company']+"is not configured of type"+arrayListData[0][p]['mode']);
                    }
                })

        }

    }

    submitTransaction(){
        let obj={}
        obj['ratePerHr']=this.formConfigData['ratePerHr']
        obj['ratePerKm']=this.formConfigData['ratePerKm']
        obj['tsno']=this.formConfigData['TSNo']
        obj['totalDaysPerHour']=this.formConfigData['totalDaysPerHour']
        obj['totalKms']=this.formConfigData['totalKms']
       /* obj['bookedBy']=this.formConfigData['bookedBy']*/
        obj['transactionStartDate']=this.bulkUpload.transactionStartDate
        obj['transactionEndDate']=this.bulkUpload.transactionEndDate
        obj['vehicleType']=this.formConfigData['vehicleType']
        obj['vehicleNumber']=this.vechicleSelectedDetails['vehicleNumber']
        obj['corporateContract']=this.corporateContractType
        obj['companyTransactionSelected']=this.companyTransactionSelected['companySelectedForDropDown']
        if(this.formConfigData['totalKms']>=80){
            obj['extraKms']=this.formConfigData['totalKms']-80
        }
        else{
            obj['extraKms']=0
        }

        if(this.formConfigData['totalDaysPerHour']>=1 && this.formConfigData['totalDaysPerHour']<=5){
            obj['slabRate']=900
        }
        else{
            obj['slabRate']=1800
        }

        if(this.formConfigData['totalDaysPerHour']>=8){
            obj['extraHrs']=this.formConfigData['totalDaysPerHour']-8
        }
        else{
            obj['extraHrs']=0
        }
        obj['extraKmAmt']=this.formConfigData['ratePerKm']*obj['extraKms']
        obj['extraHrAmt']=this.formConfigData['ratePerHr']*obj['extraHrs']
        obj['tripAmt']=obj['extraKmAmt']+ obj['extraHrAmt']+ obj['slabRate'];
        console.log("88888888888888888888888888------finalll-------");
        console.log(obj);



        this.cctransactionService.saveCCTransaction(obj)
            .subscribe((data: any) => {
                this.getCCTransaction()
            });
    }

    getCCIncialConfigData(){
        this.selectedVehicleItems=""
        this.companySelected=""
        this.formConfigData=[];
        this.companyTransactionSelected=""
        this.vechicleTransactionSelected=""
        this.vechicleSelectedDetails=""
        this.vechicleSelectedVehicleType=""
    }
    getAllVechicleAsset(){
        this.assetService.getAssetlistSpecificList("Vehicle")
            .subscribe((vechicleList: any) => {
                console.log("******vechicleList*************");
                console.log(vechicleList)
                this.vechicleDropdownList =vechicleList;

            })
    }

    onvehicleSelect(vehicleDetails){
        console.log("8328472384718vehicleDetails92347891237413")
        console.log(vehicleDetails._id)
        this.assetService.getAssetDetailsByMongodbId(vehicleDetails._id)
            .subscribe((vehicleDetails: any) => {
                console.log("******vechicleList*************");
                console.log(vehicleDetails);
                this.vechicleSelectedDetails=vehicleDetails
                this.vechicleSelectedVehicleType=vehicleDetails.vehicleType

            })
    }


    onCompanySelect(companyDetails){
        this.companyTransactionSelected=companyDetails
        this.vehicleCorporateContractService.getVehicleCorporateContractByMongodbId(companyDetails['_id'])
            .subscribe((companyCC: any) => {
                console.log("this.onCompanySelect-=------------+++++++++++++++++++--------")
                console.log(companyCC[0].companySelected._id)
                console.log(this.corporateContractType)
                console.log(this.vechicleSelectedVehicleType)
                this.vehicleCorporateContractService.getcompanyCCByMongodbIdAndCCTypeAndvehicleType( companyCC[0].companySelected._id ,this.corporateContractType
                    ,this.vechicleSelectedVehicleType)
                    .subscribe((companyCCDetails: any) => {
                        if(companyCCDetails && companyCCDetails[0]){
                            companyCCDetails=companyCCDetails[0];
                            for(var g=0;g<this.corporateContractFieldsService.corporateContractTypes.length;g++){
                                if(this.corporateContractFieldsService.corporateContractTypes[g]['assetType']['typeId']==
                                    companyCCDetails.corporateContract){
                                    let CCFieldsConfiguration = Object.keys(this.corporateContractFieldsService.corporateContractTypes[g].configuration);
                                    let companyCCKeys = Object.keys(companyCCDetails);
                                    let VehicleCCKeys = Object.keys(this.vechicleSelectedDetails);
                                    if(companyCCKeys){
                                        for(var k=0;k<companyCCKeys.length;k++){
                                            for(let p=0;p<CCFieldsConfiguration.length;p++){
                                                if(companyCCKeys[k]== CCFieldsConfiguration[p]){
                                                    this.formConfigData
                                                        [this.corporateContractFieldsService.corporateContractTypes[g].configuration[CCFieldsConfiguration[p]]['field']] =
                                                        companyCCDetails[companyCCKeys[k]]
                                                }
                                            }
                                        }

                                        this.formConfigData['vehicleType']=this.vechicleSelectedVehicleType;

                                    }
                                    console.log(this.corporateContractFieldsService.corporateContractTypes[g])
                                    this.fromConfigObjectForFrom(this.corporateContractFieldsService.corporateContractTypes[g],companyCCDetails)
                                }

                            }
                        }
                        else {
                            alert("selected vehicle type  is not configured")
                        }
                    });
            })



    }

    fromConfigObjectForFrom(configDetailsObj,companyCCDetails){
        let configObj=  configDetailsObj.configuration
        let responseKeys = Object.keys(configObj);
        let formData = [];
        for ( var prop of responseKeys) {
            formData.push(configObj[prop]);
        }
        if(formData){
            this.corporateContractFields=formData;
        }


    }

    deleteCC1Trasnaction(ccDetails){
        console.log("ccDetails0000000000000werwerwer------------")
        console.log(ccDetails)
        this.cctransactionService.deleteCCTransactionByMongodbId(ccDetails._id)
            .subscribe((data: any) => {
                this.getCCTransaction()
            })
    }

    intermerdiateDeleteTranasaction(transactionData){
        this.deleteTransactionData=transactionData
    }





    getCCTransaction() {
        this.cctransactionService.getCCTransactionByType(this.corporateContractType)
            .subscribe(CCListDetails => {
                this.CC3List =CCListDetails
            });
    }



    getAllCompanyOfTypeCC3(){
        this.vehicleCorporateContractService.getCompanyByCCType(this.corporateContractType)
            .subscribe((companyList: any) => {
                console.log(companyList)
                this.companyAssetDropDown =_.uniqBy(companyList,function(companyList){ return companyList['companySelectedForDropDown']});
            })
    }

    constructor(public cctransactionService: CctransactionService,
                private assetService:AssetService,
                private vechicleTypeServiceService: VechicleTypeServiceService,
                private vehicleCorporateContractService:VehicleCorporateContractService,
                private corporateContractFieldsService:CorporateContractFieldsService,
                public corporateContractTypeService:CorporateContractTypeService,
                public excelService:ExcelService) {

    }

    ngOnInit() {
        this.getCCTransaction()
    }

    data: any = [{
        SlNo: 0,
        date: "",
        vehicleNumber: "",
        tsno:"",
        mode:0,
        company:"",
        totalKms:0,
        totalDaysPerHour:0
    }];

    exportAsXLSX():void {
        this.excelService.exportAsExcelFile(this.data, 'onDemand');
    }



}
