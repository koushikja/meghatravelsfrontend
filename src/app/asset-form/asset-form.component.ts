import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {AssetService} from "../../services/asset.service";
import {FormService} from "../../services/form.service";
import {ServiceTypeServiceService} from "../../services/service-type-service.service";
import {FuelServiceService} from "../../services/fuel-service.service";
import {LessorTypeServiceService} from "../../services/lessor-type-service.service";
import {ShiftTypeServiceService} from "../../services/shift-type-service.service";
import {ExpenseTypeServiceService} from "../../services/expense-type-service.service";
import {CorporateContractTypeService} from "../../services/corporate-contract-type.service";
import {VechicleHistoryService} from "../../services/vechicle-history.service";
import {UserManagementService} from "../../services/user-management.service";
import {CorporateContractFieldsService} from "../../services/corporate-contract-fields.service";
import {VechicleTypeServiceService} from "../../services/vechicle-type-service.service";

declare var $: any;

@Component({
    selector: 'app-asset-form',
    templateUrl: './asset-form.component.html',
    styleUrls: ['./asset-form.component.scss']
})


export class AssetFormComponent implements OnInit {
    public allAssets: any = [];
    shiftTypesDropDown:any;
    vechicleAssetsDropDown:any;
    vechicleTypesDropDown:any;
    selectedShiftType:any;
    vechicleShiftType:any;
    Object = Object;
    corporateContractType:any
    vechicleID:any
    corporateContractFields:any
    isCorporateType:boolean
    @Output() savedAssetCardToAssetList: EventEmitter<any> = new EventEmitter();
    @Input() formConfigData: any;
    @Input() assetSelectedType: any;
    @Input() mainStatusAsset: any;
    @Input() mainAsset: any;
    @Input() assetParameterConfigName: any;
    @Output() savedAssetCard: EventEmitter<any> = new EventEmitter();
    @Output() savedAssetConfig: EventEmitter<any> = new EventEmitter();

    constructor(public assetService: AssetService, public formService: FormService, public fuelServiceService: FuelServiceService,
                public serviceTypeServiceService: ServiceTypeServiceService, public lessorTypeServiceService: LessorTypeServiceService,
                public shiftTypeServiceService: ShiftTypeServiceService, public expenseTypeServiceService: ExpenseTypeServiceService
        , public corporateContractTypeService: CorporateContractTypeService,public vechicleHistoryService: VechicleHistoryService,
                public userManagementService:UserManagementService,public corporateContractFieldsService:CorporateContractFieldsService,
                public vechicleTypeServiceService:VechicleTypeServiceService,
                ) {
        console.log("----------------------")

    }

    submitConfigDetails() {
        this.formConfigData.assetType = this.assetSelectedType;
        this.assetService.saveAssetDetails(this.formService.formatAssetSaveDetails(this.formConfigData))
            .subscribe((savedAssetValue: any) => {
                this.savedAssetCard.emit(savedAssetValue);
                this.corporateContractFields=[]
            })

    }

    submitParameterConfigDetails() {
        console.log("**************-------------on submit")
        console.log(this.assetParameterConfigName)
        console.log(this.formService.formatAssetSaveDetails(this.formConfigData))
        this.formConfigData.assetType = this.assetSelectedType;
        if (this.assetParameterConfigName === "fuelType") {
            this.fuelServiceService.saveFuelTypeDetails(this.formService.formatAssetSaveDetails(this.formConfigData))
                .subscribe((savedAssetValue: any) => {
                    this.savedAssetConfig.emit(savedAssetValue);
                    $('#add').modal('hide');

                })
        }
        if (this.assetParameterConfigName === "ServiceType") {
            this.serviceTypeServiceService.saveServiceTypeDetails(this.formService.formatAssetSaveDetails(this.formConfigData))
                .subscribe((savedAssetValue: any) => {
                    this.savedAssetConfig.emit(savedAssetValue);
                    $('#add').modal('hide');

                })
        }

        if (this.assetParameterConfigName === "LessorType") {
            this.lessorTypeServiceService.saveLesserTypeDetails(this.formService.formatAssetSaveDetails(this.formConfigData))
                .subscribe((savedAssetValue: any) => {
                    this.savedAssetConfig.emit(savedAssetValue);
                    $('#add').modal('hide');

                })
        }

        if (this.assetParameterConfigName === "ShiftTypeCost") {
            this.shiftTypeServiceService.saveShiftTypeCostDetails(this.formService.formatAssetSaveDetails(this.formConfigData))
                .subscribe((savedAssetValue: any) => {
                    this.savedAssetConfig.emit(savedAssetValue);
                    $('#add').modal('hide');

                })
        }

        if (this.assetParameterConfigName === "ExpenseType") {
            this.expenseTypeServiceService.saveExpenseTypeDetails(this.formService.formatAssetSaveDetails(this.formConfigData))
                .subscribe((savedAssetValue: any) => {
                    this.savedAssetConfig.emit(savedAssetValue);
                    $('#add').modal('hide');

                })
        }


        if (this.assetParameterConfigName === "corporateContractType") {
            this.corporateContractTypeService.saveCorporateContractTypeDetails(this.formService.formatAssetSaveDetails(this.formConfigData))
                .subscribe((savedAssetValue: any) => {
                    this.savedAssetConfig.emit(savedAssetValue);
                    $('#add').modal('hide');

                })
        }


        if (this.assetParameterConfigName === "VehicleType") {
            this.vechicleTypeServiceService
                .saveVechicleTypeDetails(this.formService.formatAssetSaveDetails(this.formConfigData))
                .subscribe((savedAssetValue: any) => {
                    this.savedAssetConfig.emit(savedAssetValue);
                    $('#add').modal('hide');

                })
        }


        if (this.assetParameterConfigName === "vechicleHistoryType") {
            console.log("*****vechicleID*****")
            console.log(this.vechicleID)
            let forobj=this.formService.formatAssetSaveDetails(this.formConfigData)
            forobj['vechicleID']=this.vechicleID
            this.vechicleHistoryService.saveVechicleHistoryDetails(forobj)
                .subscribe((savedAssetValue: any) => {
                    this.savedAssetConfig.emit(savedAssetValue);
                    $('#add').modal('hide');

                })
        }
        if (this.assetParameterConfigName === "userConfig") {
            console.log("**********")
            this.userManagementService.saveUserDetails(this.formService.formatAssetSaveDetails(this.formConfigData))
                .subscribe((savedAssetValue: any) => {
                    this.savedAssetConfig.emit(savedAssetValue);
                    $('#add').modal('hide');

                })
        }


    }

    showVechicleId(vechicledetails){
        console.log("****contractType******")
        console.log(vechicledetails)
        // this.vechicleID=vechicledetails.

    }

    showCorporateContractType(contractType){
        this.getAllShiftType();
        this.getAllVechicleType()
        this.isCorporateType=true
        console.log("****contractType******")
        console.log(this.corporateContractType)
        for(var p=0;p<this.corporateContractFieldsService.corporateContractTypes.length;p++){
            if(this.corporateContractFieldsService.corporateContractTypes[p].assetType.typeId===
                this.corporateContractType){
                        let configObj=  this.corporateContractFieldsService.corporateContractTypes[p].configuration
                        let responseKeys = Object.keys(configObj);
                        let formData = [];
                        for ( var prop of responseKeys) {
                            formData.push( configObj[prop]);
                        }
                   /*     let obj= {

                                "field":"shiftType",
                                "type" : "dropDown",
                                "description" : "Select shift Type",
                                "label":"Shift Type",
                                "required":false,
                                "autoComplete":true,
                                "fieldDisplayType":"dropDown",
                                "value":{
                                    "dropDownValues": this.shiftTypesDropDown,
                                    "defaultValue":""
                            }
                        }
                        formData.push(obj);*/
                        console.log("*888888888888formData")
                        console.log(formData)
                         this.corporateContractFields=formData
                break
                }
                else{
                this.corporateContractFields=[]
            }
            }
        }

    ngOnInit() {
        this.getAllVechicle();
    }

    getAllVechicle(){
        this.vechicleTypeServiceService.getAllVechicle()
            .subscribe((vechicleAssets: any) => {
                console.log("--------shiftTypes-----getAllShiftType--")
                console.log(vechicleAssets)
                this.vechicleAssetsDropDown=vechicleAssets
            })
    }

    getAllShiftType(){
        this.shiftTypeServiceService.getShiftTypeCostDropDown()
            .subscribe((shiftTypes: any) => {
                console.log("--------shiftTypes-----getAllShiftType--")
             console.log(shiftTypes)
                this.shiftTypesDropDown=shiftTypes
            })
    }


    getAllVechicleType(){
        this.vechicleTypeServiceService.getAllVechicleTypeDropdown()
            .subscribe((vechicleTypes: any) => {
                console.log("--------shiftTypes-----getAllShiftType--")
                console.log(vechicleTypes)
                this.vechicleTypesDropDown=vechicleTypes
            })
    }

}
