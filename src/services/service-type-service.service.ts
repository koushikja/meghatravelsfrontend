import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {ConfigService} from "./config.service";

@Injectable()
export class ServiceTypeServiceService {

    constructor(private http: HttpClient, public configService: ConfigService) {
    }

    saveServiceTypeDetails(serviceTypeData) {
        return this.http.post(this.configService.appConfig.appBaseUrl + 'serviceDetails', serviceTypeData);
    }

    getAllServiceType(skip, limit) {
        return this.http.get(this.configService.appConfig.appBaseUrl + 'allServiceDetails' + '/' + skip + '/' + limit);
    }

    getServiceTypeByMongodbId(serviceMongoId) {
        return this.http.get(this.configService.appConfig.appBaseUrl + 'serviceBymongoId/' + serviceMongoId);
    }

    updateServiceTypeById(serviceTypeId, serviceDetails) {
        return this.http.post(this.configService.appConfig.appBaseUrl + 'editServiceBymongoId/' + serviceTypeId, serviceDetails);
    }


    deleteServiceTypeByMongodbId(serviceMongoId) {
        return this.http.delete(this.configService.appConfig.appBaseUrl + 'serviceBymongoId/' + serviceMongoId);
    }

    getServiceTypeCount() {
        return this.http.get(this.configService.appConfig.appBaseUrl + 'serviceType/count');
    }




}
