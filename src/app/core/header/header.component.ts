import {Component, OnInit} from '@angular/core';
import {Router} from "@angular/router";


@Component({
    selector: 'app-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
    currRoute: string;

    constructor(private router: Router) {
        this.currRoute = router.url;
    }

    ngOnInit() {
    }

    logout() {
        this.router.navigate(['login']);
    }
}
