import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {ConfigService} from "./config.service";

@Injectable()
export class UserManagementService {

    constructor(private http: HttpClient, public configService: ConfigService) {
    }

    saveUserDetails(userData) {
        return this.http.post(this.configService.appConfig.appBaseUrl + 'addUserDetails', userData);
    }

    getAllUser(skip, limit) {
        return this.http.get(this.configService.appConfig.appBaseUrl + 'allUsers' + '/' + skip + '/' + limit);
    }

    getUserByMongodbId(usertMongoId) {
        return this.http.get(this.configService.appConfig.appBaseUrl + 'userById/' + usertMongoId);
    }

    updateUserById(userId, userTypeData) {
        return this.http.post(this.configService.appConfig.appBaseUrl + 'editUserDetails/'  + userId, userTypeData);
    }


    deleteUserByMongodbId(shiftMongoId) {
        return this.http.delete(this.configService.appConfig.appBaseUrl + 'userByMongoId/' + shiftMongoId);
    }

    getUserCount() {
        return this.http.get(this.configService.appConfig.appBaseUrl + 'allUsers/count');
    }



    sendEmail(userEmailID) {
        var obj={}
        obj['email']=userEmailID
        return this.http.post(this.configService.appConfig.appBaseUrl + 'forgotPassword', obj);
    }


    notifyUserEmail(userMongodbId) {
        return this.http.get(this.configService.appConfig.appBaseUrl + 'notifyUserPassword/'+userMongodbId);
    }


}
