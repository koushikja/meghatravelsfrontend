import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VehicleSiftCorporteAssociation2Component } from './vehicle-sift-corporte-association2.component';

describe('VehicleSiftCorporteAssociation2Component', () => {
  let component: VehicleSiftCorporteAssociation2Component;
  let fixture: ComponentFixture<VehicleSiftCorporteAssociation2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VehicleSiftCorporteAssociation2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VehicleSiftCorporteAssociation2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
