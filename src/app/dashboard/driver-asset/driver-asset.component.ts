import { Component, OnInit } from '@angular/core';
import {AssetService} from "../../../services/asset.service";

@Component({
  selector: 'app-driver-asset',
  templateUrl: './driver-asset.component.html',
  styleUrls: ['./driver-asset.component.scss']
})
export class DriverAssetComponent implements OnInit {
    isEdit:boolean=false;
    assetType : string = "Driver";
    driverAssetList:any=[];
    driverDetails:any={}
    driverEditDetails:any={}
    constructor(public assetService:AssetService) { }

    ngOnInit() {
        this.getAllAssets()
    }
    assetdriverAdded(){
        this.getAllAssets()
    }


    getAllAssets(){
        this.assetService.getAssetlistSpecificList( this.assetType )
            .subscribe((assetList: any) => {
                this.driverAssetList=assetList
            })
    }

    intermerdiateDeleteTranasaction(driverDetail){
        this.isEdit=true
        this.driverDetails=driverDetail
    }
    deletedriverDetails(driverDetail){
        this.assetService.deleteAssetDetailsByMongodbId(driverDetail._id)
            .subscribe((assetList: any) => {
                this.deleteAssetCache(driverDetail)
            })
    }

    deleteAssetCache(driverDetail){
        for(let j=0;j<this.driverAssetList.length;j++){
            if(this.driverAssetList[j]._id===driverDetail['_id']){
                this.driverAssetList.splice(j,1);
            }
        }
    }
    editIntermediatedriver(driverDetail){
        this.isEdit=true
        this.driverEditDetails =driverDetail
    }

    addAsset(){
        this.isEdit=false
    }

}
