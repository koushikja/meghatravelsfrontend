import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DriverAssetComponent } from './driver-asset.component';

describe('DriverAssetComponent', () => {
  let component: DriverAssetComponent;
  let fixture: ComponentFixture<DriverAssetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DriverAssetComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DriverAssetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
